module bitbucket.org/amirhosseinsobhani/book-store-golang.git

go 1.16

require (
	github.com/kelseyhightower/envconfig v1.4.0 // indirect
	github.com/nicksnyder/go-i18n/v2 v2.1.2 // indirect
	github.com/pelletier/go-toml v1.9.1 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
