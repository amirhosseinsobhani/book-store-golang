package i18n

import (
	"bitbucket.org/amirhosseinsobhani/book-store-golang.git/internal/contract"
	translator "github.com/nicksnyder/go-i18n/v2/i18n"
	"github.com/pelletier/go-toml"
	"golang.org/x/text/language"
)

type messageBundle struct {
	bundle *translator.Bundle
}

func New(path string) (contract.Translator, error) {
	bundle := &messageBundle{
		bundle: translator.NewBundle(language.English),
	}

	if err := bundle.loadBundle("build/i18n"); err != nil {
		return nil, err
	}

	return bundle, nil
}

func (m *messageBundle) loadBundle(path string) error {
	m.bundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)

	_, err := m.bundle.LoadMessageFile(path + "message.en.toml")
	if err != nil {
		return err
	}

	_, err = m.bundle.LoadMessageFile(path + "message.fa.toml")
	if err != nil {
		return err
	}

	return nil
}

func (m messageBundle) getLocalized(lang string) *translator.Localizer {
	return translator.NewLocalizer(m.bundle, lang)
}

func (m *messageBundle) TranslateEn(lang string, key string) string {
	msg, err := m.getLocalized(lang).Localize(&translator.LocalizeConfig{
		MessageID: key,
	})

	if err != nil {
		return key
	}

	return msg
}

func (m *messageBundle) Translate(key string) string {
	msg, err := m.getLocalized("en").Localize(&translator.LocalizeConfig{
		MessageID: key,
	})

	if err != nil {
		return key
	}

	return msg
}
