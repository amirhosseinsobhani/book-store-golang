package errors

import "net/http"

type (
	kind uint

	serverError struct {
		kind    kind
		message string
	}
)

const (
	_ kind = iota
	KindInvalid
	KindNotFound
	KindUnAuthorized
	KindUnexpected
	KindNotAllowed
)

var (
	httpError = map[kind]uint{
		KindInvalid:      http.StatusBadRequest,
		KindNotFound:     http.StatusNotFound,
		KindUnAuthorized: http.StatusUnauthorized,
		KindUnexpected:   http.StatusInternalServerError,
		KindNotAllowed:   http.StatusMethodNotAllowed,
	}
)

func New(k kind, m string) error {
	return serverError{
		kind:    k,
		message: m,
	}
}

func (se serverError) Error() string {
	return se.message
}

func HttpError(err error) (string, uint) {
	serverError, ok := err.(serverError)

	if !ok {
		return err.Error(), http.StatusBadRequest
	}

	code, ok := httpError[serverError.kind]

	if !ok {
		return serverError.message, http.StatusBadRequest
	}

	return serverError.message, code
}
