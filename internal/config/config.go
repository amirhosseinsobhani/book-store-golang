package config

type (
	Config struct {
		Database Database `yaml:"database"`
		I18N     I18N     `yaml:"i18n"`
	}

	Database struct {
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		DBName   string `yaml:"db_name"`
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		SSL      string `yaml:"ssl"`
		TimeZone string `yaml:"time_zone"`
		Charset  string `yaml:"charset"`
	}

	I18N struct {
		BundlePath string `yaml:"bundle_path"`
	}
)
