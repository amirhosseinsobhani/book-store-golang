package contract

type (
	Translator interface {
		Translate(key string) string
		TranslateEn(lang string, key string) string
	}
)
