package application

import (
	"bitbucket.org/amirhosseinsobhani/book-store-golang.git/internal/config"
	"bitbucket.org/amirhosseinsobhani/book-store-golang.git/pkg/translator/i18n"
)

func Run(cfg *config.Config) error {
	translator, err := i18n.New(cfg.I18N.BundlePath)

	if err != nil {
		return err
	}

	_ = translator

	return nil
}